package com.dcode.groupscan.ui.view;

import android.content.Context;
import android.content.ContextWrapper;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.dcode.groupscan.common.AppConstants;
import com.dcode.groupscan.common.PermissionsManager;
import com.dcode.groupscan.data.model.MENU;
import com.dcode.groupscan.ui.adapter.ObjectsAdapter;
import com.dcode.groupscan.R;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;


public class HomeFragment extends BaseFragment implements View.OnClickListener {

    View root;
    ObjectsAdapter detAdapter;



    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);


        root = inflater.inflate(R.layout.fragment_home, container, false);

                RecyclerView recyclerView = root.findViewById(R.id.recyclerView);
        ((DefaultItemAnimator) Objects.requireNonNull(recyclerView.getItemAnimator())).setSupportsChangeAnimations(false);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));



        List menuList = new ArrayList<MENU>();
        MENU menu = new MENU();
        menu.MENU_ID = 1;
        menu.MENU_CODE = "IN";
        menu.MENU_NAME = "Serial Packing";
        menu.MENU_DESC = "Scanning group items";
        menuList.add(menu);

//        menu = new MENU();
//        menu.MENU_ID = 2;
//        menu.MENU_CODE = "OUT";
//        menu.MENU_NAME = "Outbound";
//        menu.MENU_DESC = "Outbound Transactions";
//        menuList.add(menu);
//
//        menu = new MENU();
//        menu.MENU_ID = 3;
//        menu.MENU_CODE = "BT";
//        menu.MENU_NAME = "Bin to Bin Transfer";
//        menu.MENU_DESC = "Bin Transfer Transactions";
//        menuList.add(menu);
//
//        menu = new MENU();
//        menu.MENU_ID = 4;
//        menu.MENU_CODE = "ST";
//        menu.MENU_NAME = "Stock Take";
//        menu.MENU_DESC = "Stock Take Transactions";
//        menuList.add(menu);


        detAdapter = new ObjectsAdapter(getContext(),menuList,  this);
        recyclerView.setAdapter(detAdapter);

        PermissionsManager.verifyWriteStoragePermissions(getActivity());
        PermissionsManager.verifyWriteStoragePermissions(getActivity());

        return root;
    }

    @Override
    public void onClick(View view) {
        MENU menu = (MENU) view.getTag();

        if (menu == null) {
            return;
        }

        showToast("menu "+menu.MENU_NAME);

        NavigateToItemDetails("");
    }

    private void NavigateToItemDetails(String scanCode) {
//        Bundle bundle = new Bundle();
//        bundle.putSerializable(AppConstants.SELECTED_OBJECT, ls_items);
//        bundle.putString(AppConstants.SELECTED_CODE, scanCode);
        ((MainActivity) requireActivity()).NavigateToFragment(R.id.nav_inbound, null);
    }


}