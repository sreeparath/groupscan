package com.dcode.groupscan.ui.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.dcode.groupscan.R;
import com.dcode.groupscan.data.model.BARCODE;
import com.dcode.groupscan.data.model.MENU;

import java.util.ArrayList;
import java.util.List;

public class BarcodeListAdapter
        extends RecyclerView.Adapter<BarcodeListAdapter.RecyclerViewHolder>
        implements Filterable {

    private Context context;
    private View.OnClickListener shortClickListener;
    private List<BARCODE> dataList;
    private ArrayList<BARCODE> dataListFull;

    private Filter filter = new Filter() {
        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            List<BARCODE> filteredList = new ArrayList<>();

            if (constraint == null || constraint.length() == 0) {
                filteredList.addAll(dataListFull);
            } else {
                String filterPattern = constraint.toString().toLowerCase().trim();

                for (BARCODE item : dataListFull) {
                    if (item.Code.toLowerCase().contains(filterPattern.toLowerCase()) ) {
                        filteredList.add(item);
                    }
                }
            }

            FilterResults results = new FilterResults();
            results.values = filteredList;

            return results;
        }

        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            dataList.clear();
            dataList.addAll((List) results.values);
            notifyDataSetChanged();
        }
    };

    public BarcodeListAdapter(List<BARCODE> detList) {
        this.dataList = detList;
        setHasStableIds(true);
    }

    public BarcodeListAdapter(Context context, List<BARCODE> objects, View.OnClickListener shortClickListener) {
        this.context = context;
        this.shortClickListener = shortClickListener;
        this.dataList = objects;
        this.dataListFull = new ArrayList<>(objects);
        setHasStableIds(true);
    }

    @Override
    public Filter getFilter() {
        return filter;
    }

    @NonNull
    @Override
    public RecyclerViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new RecyclerViewHolder(LayoutInflater.from(parent.getContext())
                .inflate(R.layout.layout_card_barcode, parent, false));
    }

    @Override
    public void onBindViewHolder(final RecyclerViewHolder holder, int position) {
        final BARCODE objectItem = this.dataList.get(position);

        holder.menuText.setText(objectItem.Code);
        holder.itemView.setTag(objectItem);
        holder.itemView.setOnClickListener(shortClickListener);


    }

    @Override
    public int getItemCount() {
        return dataList.size();
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    public  void clearAdapter(){
        this.dataList = new ArrayList<>();
        this.dataListFull = new ArrayList<>();
        notifyDataSetChanged();
    }

    public void addItems(List<BARCODE> list) {
        this.dataList = list;
        this.dataListFull = new ArrayList<>(list);

        notifyDataSetChanged();
    }

    public List<BARCODE> getItems() {
        return this.dataList;
    }



    static class RecyclerViewHolder extends RecyclerView.ViewHolder {
        TextView menuText;
        TextView menuDesc;
        LinearLayout borderPanel;

        RecyclerViewHolder(View view) {
            super(view);
            menuText = view.findViewById(R.id.menuText);
            menuDesc = view.findViewById(R.id.menuDesc);
            //borderPanel = view.findViewById(R.id.borderPanel);
        }
    }

}
