package com.dcode.groupscan.ui.view;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.InputType;
import android.util.Log;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.ImageButton;


import com.dcode.groupscan.App;
import com.dcode.groupscan.common.PermissionsManager;
import com.dcode.groupscan.data.model.COMPANY;
import com.dcode.groupscan.R;
import com.google.android.material.textfield.TextInputEditText;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;

import java.util.ArrayList;
import java.util.List;




public class LoginActivity
        extends BaseActivity {
    private ImageButton imgSettings;
    private Button btSignIn;
    private TextInputEditText edUserName;
    private TextInputEditText edPassword;
    private TextInputEditText edWorkSites;
    //    private ArrayAdapter<LOCATIONS> locationsAdapter;
    private ArrayAdapter<COMPANY> companyAdapter;
    private List<COMPANY> companyList;
    private AutoCompleteTextView acCompany;
    private boolean isSignClicked = false;
    private boolean isUserValid = false;

    @Override
    protected int getLayoutResourceId() {
        return R.layout.activity_login;
    }



    @Override
    protected void onViewReady(Bundle savedInstanceState) {
        imgSettings = findViewById(R.id.imgSettings);
        imgSettings.setOnClickListener(v -> onSettingsClick());
        edUserName = findViewById(R.id.edUserName);
        edPassword = findViewById(R.id.edPassword);

        btSignIn = findViewById(R.id.btSignIn);
        btSignIn.setOnClickListener(v -> OnSignInClick());

        //GetCompanies();


    }

    @Override
    public void onBackPressed() {
        finishAndRemoveTask();
        super.onBackPressed();
    }

//    @Override
//    public void onActivityResult(int requestCode, int resultCode, Intent intent) {
//        super.onActivityResult(requestCode, resultCode, intent);
//
//        if (resultCode != RESULT_OK) {
//            return;
//        }
//
//        if (requestCode == AppConstants.LOCATION) {
//            LOCATIONS selectedObject = (LOCATIONS) intent.getSerializableExtra(AppConstants.SELECTED_OBJECT);
//            App.currentLocation = selectedObject;
//            if (selectedObject != null && selectedObject.LOCODE.length() > 0) {
//                edWorkSites.setText(selectedObject.LONAME);
//                btSignIn.setText(R.string.next);
//            }
//        }
//    }

    private void OnSignInClick() {
        String errMessage = "";
        edUserName.setError(null);
        edPassword.setError(null);



//        if (App.currentCompany != null && App.currentLocation != null && isSignClicked && isUserValid) {
//            //showToast(App.currentCompany.Code + " ## "+App.currentLocation.LOCODE);
//            GetAppSettings();
//            return;
//        }

        if (edUserName.getText().length() < 1) {
            errMessage = String.format("%s required..", getString(R.string.username));
            showToast(errMessage);
            edUserName.setError(errMessage);
            return;
        }

        if (edPassword.getText().length() < 1) {
            errMessage = String.format("%s required..", getString(R.string.password));
            showToast(errMessage);
            edPassword.setError(errMessage);
            return;
        }

        GetAppSettings();

/*        if (App.currentLocation == null) {
            showToast(String.format("%s required..", getString(R.string.work_site)));
            return;
        }*/

//        if (App.currentCompany == null) {
//            errMessage = String.format("%s required..", getString(R.string.company));
//            showToast(errMessage);
//            acCompany.setError(errMessage);
//            return;
//        }

//        String loginName = edUserName.getText().toString();
//        String passWd = edPassword.getText().toString().trim();

       // showProgress(false);
        //ValidateUserOnline(loginName, passWd, passWd.length());
    }





/*    private void GetLocations() {
        if (App.currentCompany == null || App.currentCompany.Code.length() <= 0) {
            showToast("Please sign-in to select location");
        }

        if (Utils.GetRowsCount("LOCATIONS", true, "COCODE", App.currentCompany.Code) == 0) {
            GetLocationsOnline();
        } else {
            Intent intent = new Intent(this, LocationSearchActivity.class);
            startActivityForResult(intent, AppConstants.LOCATION);
        }
    }

    private void GetLocationsOnline() {
        // download the locations
        try {
            showProgress(false);
            JsonObject requestObject = ServiceUtils.MastersDownload.getLocationsObject();
            App.getNetworkClient().getAPIService().getGenericRet(requestObject, new Callback<GenericRetResponse>() {
                @Override
                public void success(GenericRetResponse genericRetResponse, Response response) {
                    if (genericRetResponse.getErrCode().equals("S")) {
                        LOCATIONS[] locations = new Gson().fromJson(genericRetResponse.getXmlDoc(), LOCATIONS[].class);
                        for (LOCATIONS location : locations) {
                            location.COCODE = App.currentCompany.Code;
                        }
                        App.getDatabaseClient().getAppDatabase().genericDao().deleteAllLocationsByCompany(App.currentCompany.Code);
                        App.getDatabaseClient().getAppDatabase().genericDao().insertLocations(locations);
                        GetLocations();
                    } else {
                        String msg = genericRetResponse.getErrMessage();
                        Log.d(App.TAG, msg);
                        showToast(msg);
                        updateProgressMessage("Download request failed.");
                    }
                }

                @Override
                public void failure(RetrofitError error) {
                    String msg = error.getMessage();
                    Log.d(App.TAG, msg);
                    showToast(msg);
                }
            });
        } catch (Exception e) {
            String msg = e.getMessage();
            Log.d(App.TAG, msg);
            showToast(msg);
        } finally {
            dismissProgress();
        }
    }*/

    private void GetAppSettings() {

        Intent intent = new Intent(getApplicationContext(), MainActivity.class);
        startActivity(intent);
        finish();
    }

    private void onSettingsClick() {
//        Intent intent = new Intent(com.dcode.inventory.ui.view.LoginActivity.this, SettingsActivity.class);
//        startActivity(intent);
    }

    private void GetCompanies() {
//        companyList = new ArrayList<COMPANY>();
//        COMPANY company;
//
//        company = new COMPANY();
//        company.Code = "01";
//        company.Name = "SAWHNEY FOODSTUFF TRADING";
//        company.DbName = "SAFCO";
//        companyList.add(company);
//
//        company = new COMPANY();
//        company.Code = "02";
//        company.Name = "SAFCO INTERNATIONAL TRADING";
//        company.DbName = "SAFCOINT";
//        companyList.add(company);
//
//        company = new COMPANY();
//        company.Code = "03";
//        company.Name = "SFG GENERAL TRADING";
//        company.DbName = "SFG";
//        companyList.add(company);

        //companyAdapter = new ArrayAdapter<>(com.dcode.inventory.ui.view.LoginActivity.this, R.layout.dropdown_menu_popup_item, companyList);
        //acCompany.setAdapter(companyAdapter);
        //acCompany.setThreshold(100);
    }
}