package com.dcode.groupscan.ui.view;

import android.media.MediaPlayer;
import android.media.MediaScannerConnection;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;


import com.dcode.groupscan.App;
import com.dcode.groupscan.common.AppConstants;
import com.dcode.groupscan.R;
import com.dcode.groupscan.data.model.BARCODE;
import com.dcode.groupscan.ui.adapter.BarcodeAdapter;
import com.dcode.groupscan.ui.adapter.BarcodeListAdapter;
import com.google.android.material.button.MaterialButton;
import com.google.android.material.textfield.TextInputEditText;
import com.google.gson.Gson;
import com.google.gson.JsonObject;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.TreeMap;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

public class GroupScanFragment extends BaseFragment {
    private TextInputEditText edDocNo,edBarcode,edQty,edFilename;
    private TextView message;
//    private AutoCompleteTextView tvDocTypes;
//    private ArrayAdapter<DocType> objectsAdapter;
    private int promptResource;
    private String DocNo;
    private long HDR_ID;
    private RecyclerView recyclerView;
    private ArrayAdapter<BARCODE> objectsAdapter;
    private BarcodeAdapter batchesAdapter;
    //private BarcodeListAdapter batchesAdapter;
    private HashMap<String,String> dataList;
    private List<BARCODE> dataArray;
    private ImageButton btnSort;


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater,
                             @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);

        if (this.getArguments() != null) {
            ModuleID = this.getArguments().getInt("ModuleID", 0);
        }



        View root = inflater.inflate(R.layout.fragment_inbound_no, container, false);
        //ImageView imgDocNo = root.findViewById(R.id.imgDocNo);
        //imgDocNo.setImageResource(R.drawable.ic_005);

        dataList = new HashMap<>();


        edDocNo = root.findViewById(R.id.edScanCode);
        message = root.findViewById(R.id.message);
        //edDocNo.requestFocus();
        edQty = root.findViewById(R.id.edQty);
        edFilename = root.findViewById(R.id.edFilename);

        edBarcode = root.findViewById(R.id.edBarcode);
        edBarcode.setOnKeyListener((v, keyCode, event) -> {
            if(keyCode == KeyEvent.KEYCODE_ENTER){
                if(event.getAction() == KeyEvent.ACTION_DOWN){
                    return onScanCodeKeyEvent();
                }else{
                    return true;
                }
            }else{
                return false;
            }
        });

        MaterialButton btPhoto = root.findViewById(R.id.btnPrint);
        btPhoto.setOnClickListener(v -> resetUI());

        MaterialButton btnPrint = root.findViewById(R.id.btnNext);
        btnPrint.setOnClickListener(v -> scanComplete());


        btnSort = root.findViewById(R.id.btnSort);
        btnSort.setOnClickListener(v -> sortList());

        recyclerView = root.findViewById(R.id.recyclerView);
        ((DefaultItemAnimator) Objects.requireNonNull(recyclerView.getItemAnimator())).setSupportsChangeAnimations(false);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));


        batchesAdapter = new BarcodeAdapter(new HashMap<>());
        //batchesAdapter = new BarcodeListAdapter(new ArrayList<>());
        recyclerView.setAdapter(batchesAdapter);

        //batchesAdapter.addItems(dataArray);


        return root;
    }

    private void sortList(){
//        List list = new LinkedList(dataList.entrySet());
//        Collections.sort(list, new Comparator() {
//            @Override
//            public int compare(Object o, Object t1) {
//                return ((Comparable) ((Map.Entry) (o)).getValue()).compareTo(((Map.Entry) (t1)).getValue());
//            }
//        });
        List<Map.Entry<String , String>> list = new ArrayList<>(dataList.entrySet());
        //list.sort(Comparator.comparing(Map.Entry::getKey));
                Collections.sort(list, new Comparator() {
            @Override
            public int compare(Object o, Object t1) {
                return ((Comparable) ((Map.Entry) (o)).getKey()).compareTo(((Map.Entry) (t1)).getKey());
            }
        });
        LinkedHashMap<String, String> sortedMap = new LinkedHashMap<>();
        list.forEach(e -> sortedMap.put(e.getKey(), e.getValue()));
        Log.d("dataList## ",sortedMap.toString());
        batchesAdapter.addItems((HashMap<String, String>) sortedMap);
    }

    private void scanComplete(){
        //showToastLong("Server Configuration Pending!");

        HashMap<String,String>   items =batchesAdapter.getItems();

        if(items.size()<=0){
            showAlert("Error","No items scanned");
        }

        String fileName = edFilename.getText().toString();
        if(fileName.length()<=0){
            showAlert("Error","File name shouldn't be empty!");
            return;
        }

        if(WriteToFile(fileName,items)){
            showToastLong("File Exported Successfully");
            resetUI();
        }

        //batchesAdapter.addItems(new ArrayList<>());
//        for ( BARCODE bar : dataArray ) {
//            Log.d("bar## ",bar.Code);
//        }
//        Collections.sort(dataArray, new Comparator<BARCODE>() {
//            @Override
//            public int compare(BARCODE lhs, BARCODE rhs) {
//                // -1 - less than, 1 - greater than, 0 - equal, all inversed for descending
//                return lhs.Code.compareTo(rhs.Code);
//            }
//        });
       // batchesAdapter.addItems(dataList);

    }

    private boolean WriteToFile(String fileName,HashMap<String,String>   scanItems) {
        try {

            String sOutFilesPath = getString(R.string.def_out_path);
            File filePath = new File(sOutFilesPath);
            if (!filePath.exists()) {
                boolean mkdir = filePath.mkdir();

                if (!mkdir) {
                    showToast("Failed: create output folder");
                    return false;
                }
            }

            String itemsFileName = String.format("%s/%s.txt", filePath,  fileName);

            try {
                if (scanItems.size() > 0) {
                    Writer itemsOutput = new BufferedWriter(new FileWriter(itemsFileName, true));
                    //FileWriter itemsOutput = new FileWriter(new File(sOutFilesPath, fileName));
                    List<Map.Entry<String , String>> list = new ArrayList<>(scanItems.entrySet());
                    list.forEach(e -> {
                        try {
                            itemsOutput.append(e.getKey()).append("\r\n");
                        } catch (IOException ioException) {
                            ioException.printStackTrace();
                        }
                    });
                    itemsOutput.close();
                }
            } catch (IOException e) {
                Log.d(App.TAG, e.getMessage());
                return false;
            }
            MediaScannerConnection.scanFile(requireActivity(),
                    new String[]{itemsFileName}, null, null);
        } catch (Exception e) {
            Log.d(App.TAG, e.getMessage());
            e.printStackTrace();
            showAlert("Error", e.getMessage());
            return false;
        }

        return true;
    }

    private boolean onScanCodeKeyEvent() {
        String ScanCode = edBarcode.getText().toString();

        String qty = edQty.getText().toString();

        if (qty.isEmpty() || qty.trim().length() == 0) {
            showAlert("WARNING","Invalid Qty!");
            edBarcode.setText("");
            return false;
        }

        if(!validateQty(Integer.parseInt(qty))){
            edBarcode.setText("");
            return false;
        }

        if (ScanCode.isEmpty() || ScanCode.trim().length() == 0) {
            showToast("Invalid bar code");
            return false;
        } else {
            ParseBarCode(ScanCode);
        }
        return true;
    }

    private void ParseBarCode(String scanCode) {
        if (scanCode.length() <= 0 ) {
            return;
        }

        int qty = Integer.parseInt(edQty.getText().toString());


        //BARCODE bar = new BARCODE();

        String[] barCodeData = scanCode.split(App.BarCodeSeparator);
        String materialCode = "";

        if (barCodeData.length>1) {
            int size = barCodeData.length;
            for(int i=0;i<size;i++){
                if(validateQty(qty)) {
                    Log.d("barCodeData[i]## ",barCodeData[i]);
                    Log.d("dataList[i]## ",dataList.toString());
                    //String barcode[] = barCodeData[i].split(App.BarCodeSeparatorAscii);
                    //dataList.put(barcode[1], barcode[1]);
                    dataList.put(barCodeData[i], barCodeData[i]);
                }
            }
        }else{
            if(validateQty(qty)) {
                Log.d("barCodeData[0]## ",barCodeData[0]);
                Log.d("dataList[0]## ",dataList.toString());
                //String barcode[] = barCodeData[0].split(App.BarCodeSeparatorAscii);
                //dataList.put(barcode[1], barcode[1]);
                dataList.put(barCodeData[0],barCodeData[0]);
            }
        }



        batchesAdapter.addItems(dataList);
        message.setText("Required Qty : "+qty+" Scanned Qty : "+dataList.size());

        if(!validateQty(qty)){
            edBarcode.setText("");
            edBarcode.requestFocus();
            return ;
        }

        //batchesAdapter.notifyDataSetChanged();
        edBarcode.setText("");
        edBarcode.requestFocus();
    }

    private boolean validateQty(int qty){

        int reqSize = dataList.size();

        if(qty >0 && reqSize >0 && qty==reqSize){
            showAlert("WARNING","ALL ITEMS SCANNED SUCCESSFULLY!!!");
            ringtone();
            edBarcode.requestFocus();
            return false;
        }
        return true;
    }

    public void ringtone(){
        try {
            Uri notification = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
            Ringtone r = RingtoneManager.getRingtone(getContext(), notification);
            r.play();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    private void addBarcodes(String barcode){

//            if(dataList.size()>0){
//                for ( bar : dataList
//                ) {
//                    if(!bar.Code.equalsIgnoreCase(barcode)){
//                        dataList.put(barcode,barcode);
//                    }
//                }
//            }else{
//                dataList.put(barcode,barcode);
//            }


    }
    private void resetUI(){
        edDocNo.setText("");
        edQty.setText("");
        edBarcode.setText("");
        message.setText("");
        dataList = new HashMap<>();
        //dataArray = new ArrayList<>();
        batchesAdapter.addItems(dataList);
        batchesAdapter.notifyDataSetChanged();
        edDocNo.requestFocus();
        edFilename.setText("");

    }




}
