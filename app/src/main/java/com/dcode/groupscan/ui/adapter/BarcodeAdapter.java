package com.dcode.groupscan.ui.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.dcode.groupscan.R;
import com.dcode.groupscan.data.model.BARCODE;
import com.dcode.groupscan.data.model.MENU;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class BarcodeAdapter
        extends RecyclerView.Adapter<BarcodeAdapter.RecyclerViewHolder>
        implements Filterable {

    private Context context;
    private View.OnClickListener shortClickListener;
    private HashMap<String,String> dataList;
    private HashMap<String,String> dataListFull;
    private List<String> dataArray;

    private Filter filter = new Filter() {
        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            HashMap<String,String> filteredList = new HashMap<>();

            if (constraint == null || constraint.length() == 0) {
                filteredList.putAll(dataListFull);
            } else {
                String filterPattern = constraint.toString().toLowerCase().trim();

//                for (BARCODE item : dataListFull) {
//                    if (item.MENU_NAME.toLowerCase().contains(filterPattern.toLowerCase()) ||
//                            item.MENU_DESC.toLowerCase().contains(filterPattern.toLowerCase())) {
//                        filteredList.add(item);
//                    }
//                }
            }

            FilterResults results = new FilterResults();
            results.values = filteredList;

            return results;
        }

        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
//            dataList.clear();
//            dataList.putAll((List) results.values);
//            notifyDataSetChanged();
        }
    };

    public BarcodeAdapter(HashMap<String,String> detList) {
        this.dataList = detList;
        //this.dataArray = dataArray;
        setHasStableIds(true);
//        grDetListFull = new ArrayList<>(detList);
    }

    public BarcodeAdapter(Context context, HashMap<String,String> objects, View.OnClickListener shortClickListener) {
        this.context = context;
        this.shortClickListener = shortClickListener;
        this.dataList = objects;
        this.dataListFull = objects;
        setHasStableIds(true);
    }

    @Override
    public Filter getFilter() {
        return filter;
    }

    @NonNull
    @Override
    public RecyclerViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new RecyclerViewHolder(LayoutInflater.from(parent.getContext())
                .inflate(R.layout.layout_card_barcode, parent, false));
    }

    @Override
    public void onBindViewHolder(final RecyclerViewHolder holder, int position) {
//        final String objectItem = this.dataList.get(dataArray.get(position));
//
//        holder.menuText.setText(objectItem);
//        holder.itemView.setTag(objectItem);
//        holder.itemView.setOnClickListener(shortClickListener);

        int i = 0;
        for (HashMap.Entry<String, String> entry : dataList.entrySet()) {
            if(position == i){
                String key = entry.getKey();
                String value = entry.getValue();
                holder.menuText.setText(key);
                //holder.slno.setText(String.valueOf(position+1));

                holder.itemView.setTag(entry);
                holder.itemView.setOnClickListener(shortClickListener);
                // print your hello word here
                break;
            }
            i++;
        }

//        holder.menuText.setText(objectItem.Code);
//        //holder.menuDesc.setText(objectItem.MENU_DESC);
//
//        holder.itemView.setTag(objectItem);
//        holder.itemView.setOnClickListener(shortClickListener);

        //Random rnd = new Random();
        //int color = Color.argb(255, rnd.nextInt(256), rnd.nextInt(256), rnd.nextInt(256));

//        int color= ((int)(Math.random()*16777215)) | (0xFF << 24);
//        LayerDrawable layerDrawable = (LayerDrawable) context.getDrawable(R.drawable.border);
//        GradientDrawable gradientDrawable = (GradientDrawable) layerDrawable
//                .findDrawableByLayerId(R.id.borcol);
//        gradientDrawable.setColor(color);


    }



    @Override
    public int getItemCount() {
        return dataList.size();
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    public void addItems(HashMap<String,String>  list) {
        this.dataList = list;
        this.dataListFull = list;
        //this.dataArray = dataArray;
        notifyDataSetChanged();
    }

    public HashMap<String,String>  getItems() {
        return this.dataList;
    }

    static class RecyclerViewHolder extends RecyclerView.ViewHolder {
        TextView menuText;
        //TextView slno;
        LinearLayout borderPanel;

        RecyclerViewHolder(View view) {
            super(view);
            menuText = view.findViewById(R.id.menuText);
            //slno = view.findViewById(R.id.slno);
            //borderPanel = view.findViewById(R.id.borderPanel);
        }
    }

}
