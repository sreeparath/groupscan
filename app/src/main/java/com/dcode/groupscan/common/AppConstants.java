package com.dcode.groupscan.common;

public class AppConstants {

    public static final int DEFAULT = 65534;
    public static final int UPLOADS = 69;
    public static final int DOWNLOADS = 96;
    public static final int USERS = 99;
    public static final int LOCATION = 1001;
    public static final int ORTYP = 1002;
    public static final int RECTYP = 1003;
    public static final int GOODS_RECEIPT = 2002;
    public static final int MOVEMENT = 2003;
    public static final int REASON = 1004;

    public static final String SIGN_BASE64 = "SIGN_BASE64";
    public static final String SIGN_PNG = "SIGN_PNG";
    public static final String SIGN_NAME = "SIGN_NAME";
    public static final String REQUEST_CODE = "REQUEST_CODE";
    public static final String MODULE_ID = "MODULE_ID";
    public static final String MASTER_ID = "MASTER_ID";
    public static final String SELECTED_ID = "SELECTED_ID";
    public static final String SELECTED_CODE = "SELECTED_CODE";
    public static final String SELECTED_NAME = "SELECTED_NAME";
    public static final String PARENT_ID = "PARENT_ID";
    public static final String FILTER_KEY_INT_ARRAY = "FILTER_KEY_INT_ARRAY";
    public static final String SELECTED_OBJECT = "SELECTED_OBJECT";

    public static final String CODE_TITLE = "CODE_TITLE";
    public static final String NAME_TITLE = "NAME_TITLE";
    public static final String TITLE = "TITLE";
    public static final int SLOC = 101;
    public static final int HELPER = 102;
    public static final int VEH = 103;
    public static final String SLOC_KEY = "SLOC";
    public static final String HELPER_KEY = "HELPER";
    public static final String VEH_KEY = "VEH";
    public static final float DIFF_VALUE = (float) 0.0001;

    public enum PadDirection {
        idRight,
        idLeft,
        idCenter,
    }

    public enum DatabaseEntities {
        USERS,
        LOCATION,
        ORTYP,
        RECTYP,
        BARCODE_PRINTERS,
        LABEL_FORMATS,
        GOODS_RECEIPT_HDR,
        GOODS_RECEIPT_DET,
        LOAD_SLIPS_HDR,
        LOAD_SLIPS_DET,
        PICK_LIST_ITEMS,
        PICK_LIST_ITEMS_RECEIPT,
        LOAD_SLIP_ITEMS_NEW,
        LOAD_SLIP_ITEMS_RECEIPT,
        STOCK_ITEMS_RECEIPT,
        STOCK_HEADER,
        STOCK_HEADER_ITEMS,
        STORE_TRANSFER_ITEMS,
        STORE_TRANSFER_ITEMS_RECEIPT,
        DRIVER_LIST,
        HELPER_LIST,
        VEHICLE_LIST,
        DocType,
        REASON_TYPE,
        PUR_RET_ITEMS_RECEIPT,
        SALE_RET_HDR,
        SALE_RET_DOC_DET,
        SALE_RET_DOC_HDR,
        SALE_RET_DET
    }
}
