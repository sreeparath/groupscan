package com.dcode.groupscan.data.model;

public class BARCODE {

    public String Code;

    public String Name;

    public String DbName;

    @Override
    public String toString() {
        return Name;
    }

    public BARCODE(String code, String name, String dbName) {
        Code = code;
        Name = name;
        DbName = dbName;
    }
}
