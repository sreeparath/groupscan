package com.dcode.groupscan.data.model;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

import java.io.Serializable;

@Entity(tableName = "MENU")
public class MENU implements Serializable {

    @PrimaryKey
    @ColumnInfo(name = "MENU_ID")
    public int MENU_ID;

    @ColumnInfo(name = "PARENT_MENU_ID")
    public int PARENT_MENU_ID;


    @ColumnInfo(name = "MENU_CODE")
    public String MENU_CODE;

    @ColumnInfo(name = "MENU_NAME")
    public String MENU_NAME;

    @ColumnInfo(name = "MENU_DESC")
    public String MENU_DESC;
}
